<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Proceso;

class ProcesoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $procesos=Proceso::all();
        return view('index',compact('procesos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {         
        $procesos= new Proceso();
        $procesos->numero_proceso=$request->get('numero_proceso');
        $procesos->descripcion=$request->get('descripcion');
        $procesos->sede=$request->get('sede');
        $procesos->presupuesto=$request->get('presupuesto');
        $procesos->save();
        return redirect('procesos')->with('success', 'Información añadida correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $procesos = Proceso::find($id);
        return view('edit',compact('procesos','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $procesos= Proceso::find($id);
        $procesos->numero_proceso=$request->get('numero_proceso');
        $procesos->descripcion=$request->get('descripcion');
        $procesos->sede=$request->get('sede');
        $procesos->presupuesto=$request->get('presupuesto');
        $procesos->save();
        return redirect('procesos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $procesos = Proceso::find($id);
        $procesos->delete();
        return redirect('procesos')->with('success','Eliminado el proceso correctamente');
    }
    public function busqueda(Request $request)
    {
        $input = $request->all();
        $fecha = $input['date'];
     
        if($fecha<>null){
            $procesos = Proceso::where("created_at", "LIKE", "%{$fecha}%")
                ->paginate(5);
          return view('index')->with('procesos', $procesos);
        }
        return redirect('procesos');
    }
}
