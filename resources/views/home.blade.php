@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    Bienvenido haz iniciado sesion

                    <div role="tabpanel">

                        <!-- Nav tabs -->
                     
                       <a href="{{ url('procesos') }}">{{ trans('Lista de Procesos') }}</a>    
                       
                    </div><!--tab panel-->
                  
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
