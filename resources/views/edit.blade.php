@extends('layouts.app')

@section('content')
<div class="container">
   <div class="col-md-12">
    <div class="card">
    <h2>Editar proceso</h2><br/>
    <form method="post" action="{{action('ProcesoController@update', $id)}}">
        @csrf
        <input name="_method" type="hidden" value="PATCH">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="numero_proceso">Numero de Proceso:</label>
                <input type="text" class="form-control" name="numero_proceso" value="{{$procesos->numero_proceso}}">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="descripcion">Descripción</label>
                <input type="text" class="form-control" name="descripcion" value="{{$procesos->descripcion}}">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4" style="margin-left:38px">
                <label for="sede">Sede:</label>
                <select name="sede">
                    <option value="bogota"  @if($procesos->sede=="bogota") selected @endif>Bogotá</option>
                    <option value="mexico"  @if($procesos->sede=="mexico") selected @endif>Mexico</option>
                    <option value="peru" @if($procesos->sede=="peru") selected @endif>Peru</option>
                </select>
            </div>
        </div>
        <div class="row">
                    <div class="col-md-4"></div>
                    <div class="form-group col-md-4">
                        <label for="presupuesto">Presupuesto:</label>
                        <input type="number" class="form-control" name="presupuesto" value="{{$procesos->presupuesto}}">
                    </div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4" style="margin-top:60px">
                <button type="submit" class="btn btn-success" style="margin-left:38px">Editar</button>
            </div>
        </div>
    </form>
    </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
    $("#presupuesto").on({
    "focus": function (event) {;
        $(event.target).select();
    },
    "keyup": function (event) {
        $(event.target).val(function (index, value ) {
            return value.replace(/\D/g, "")
                        .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
        });
    }
    });

    var $input = $('#descripcion')
    $input.keyup(function(e) {
    var max = 200;
    if ($input.val().length > max) {
        $input.val($input.val().substr(0, max));
    }
   });


    var $input = $('#numero_proceso')
    $input.keyup(function(e) {
    var max = 8;
    if ($input.val().length > max) {
        $input.val($input.val().substr(0, max));
    }
   });
});
</script>
@endsection