@extends('layouts.app')

@section('content')

<script type="text/javascript">
    $('#datepicker').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy'
    });
</script>
<div class="container">
        <div class="col-md-12">
            <div class="card">
            <br />
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div><br />
            @endif
            <h1 style="margin: 1rem 1rem">Lista de Procesos</h1>
            <div class="col-md-12">
                <td><a href="{{action('ProcesoController@create')}}" class="btn btn-success" style="padding: 1rem 2rem;margin-bottom: 3rem">Crear nuevo proceso</a></td>

                <td> 
                    <div class="panel panel-success">
                            <div class="panel-heading">Buscador</div>
                           <form action="{{url('fecha/buscar')}}" method="get">
                            <div class="panel-body">
                                <label class="label-control">Fecha de creación</label>
                                 <input class="date form-control"  type="text" id="datepicker" name="date" placeholder="2018-10-18" required="required">
                                <br>

                            </div>
                            <div class="panel-footer">
                                <button type="submit" class="btn btn-success">buscar</button>
                            </div>
                            </form>
                    </div>
                </td>
            </div>
            <div class="col-md-12">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Numero de Proceso</th>
                        <th>Descripción</th>
                        <th>Fecha de creación</th>
                        <th>Sede</th>
                        <th>Presupuesto pesos</th>
                        <th>Presupuesto en Dolares</th>
                        <th colspan="2">Acción</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($procesos as $proceso)
                        <tr>
                            <td>{{$proceso['id']}}</td>
                            <td>{{$proceso['numero_proceso']}}</td>
                            <td>{{$proceso['descripcion']}}</td>
                            <td>{{$proceso['created_at']}}</td>                      
                            <td>{{$proceso['sede']}}</td>
                            <td>{{$proceso['presupuesto']}}</td>
                             <td>{{$proceso['presupuesto']/3000}}</td>


                            <td><a href="{{action('ProcesoController@edit', $proceso['id'])}}" class="btn btn-warning">Edit</a></td>
                            <td>
                                <form action="{{action('ProcesoController@destroy', $proceso['id'])}}" method="post">
                                    @csrf
                                    <input name="_method" type="hidden" value="DELETE">
                                    <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
    </div>

@endsection