/**
Juan Carlos Alvarez Lagos
 **/
const router = new VueRouter({
    routes: [
        {
            path: 'asignacion',component: asignaciones,
            path: 'asignacionCalculada',component: asignaciones,
        },
    ]
});

function mensaje(msj,panel) {
    $('#mensajeComputed' + panel).html('<div class="alert alert-success alert-dismissible" role="alert">' +
        ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
        ' <strong>'+msj+'</strong>' +
        '</div>');
}

var asignaciones = new Vue({
    el:'#asignacion',
    created:function () {
    },
    data:{
        acalculada:false
    },
    methods:{
        addGeneral:function (idEmpleado,idConcepto,nombreConcepto,codigoConcepto) {
            valor = $('#valor' + idConcepto).val();
            url = 'addGeneral';
            //validadion a calculos retefuente
//            if(valor >= 3316000){
            if(false){
                alert('El valor debe ser menor a $3.316.000');
            }else {
                axios.post(url,{
                    'valor':valor,
                    'idEmpleado':idEmpleado,
                    'idConcepto':idConcepto,
                    'nombreConcepto':nombreConcepto
                    //parametros
                }).then(response =>{
                    //respuesta
                    console.log(response.data);
                    swal(
                        'Muy bien',
                        nombreConcepto + ' Guardado correctamente',
                        'success'
                    );
                    mensaje('Asignacion guardada correctamente','Asignacion');
                });
            }

        },
        
        addGeneralCheck: function (idEmpleado,idConcepto,nombreConcepto) {
            if ($('#valor'+ idConcepto).is(':checked') ) {
                valor = 1;
            }else {
                valor = 2;
            }
            url = 'addGeneralCheck';
            axios.post(url,{
                'valor':valor,
                'idEmpleado':idEmpleado,
                'idConcepto':idConcepto,
                'nombreConcepto':nombreConcepto
                //parametros
            }).then(response =>{
                //respuesta
                console.log(response.data);
                swal(
                    'Muy bien',
                    nombreConcepto + ' Guardado correctamente',
                    'success'
                );
                mensaje('Deduccion guardada correctamente','Deduccion');
            });
        },

       guardarAsignacion: function (idEmpleado,idConcepto) {
           valor = $('#valor' + idConcepto).val();
           url = 'asignacion';
           axios.post(url,{
               'valor':valor,
               'idEmpleado':idEmpleado,
               'idConcepto':idConcepto
               //parametros
           }).then(response =>{
               //respuesta
               console.log(response.data);
               mensaje('Asignacion guardada correctamente','Asignacion');
           });
       },guardarDeduccion: function (idEmpleado,idConcepto) {
           valor = $('#valorD' + idConcepto).val();
           url = 'deduccion';
           axios.post(url,{
               'valor':valor,
               'idEmpleado':idEmpleado,
               'idConcepto':idConcepto
               //parametros
           }).then(response =>{
               //respuesta
               console.log(response.data);
               mensaje('Deduccion guardada correctamente','Deduccion');
           });
       },
        addAcalculada:function (idEmpleado,idConcepto) {
            if ($('#aCalculada'+ idConcepto).is(':checked') ) {
                valor = 1;
            }else {
                valor = 2;
            }
            url = 'asignacionCalculada';
            axios.post(url,{
                'valor':valor,
                'idEmpleado':idEmpleado,
                'idConcepto':idConcepto
                //parametros
            }).then(response =>{
                //respuesta
                console.log(response.data);
                mensaje('Asignacion calculada guardada correctamente', 'AsignacionCalculada');
            });
        },
        addAsignacionesnofijas:function (idEmpleado,idConcepto) {
            valor = $('#valor' + idConcepto).val();
            url = 'asignacionesnofijas';
            axios.post(url,{
                'valor':valor,
                'idEmpleado':idEmpleado,
                'idConcepto':idConcepto
                //parametros
            }).then(response =>{
                //respuesta
                console.log(response.data);
                mensaje('Asignacion no fija guardada correctamente','asignacionesnofijas');
            });
        },

    }
});

