/**
 * Juan Carlos ALvarez Lagos
 *
 **/

const router = new VueRouter({
    routes: [
        {
            path: 'tipoEntidades',component: tipoEntidades,
            path: 'crearTipoEntidad',component: crearTipoEntidad,
            path: 'EditarTipoEntidad',component: editarTipoEntidad,
            path: 'EstadoTipoEntidad',component: crearTipoEntidad,
        },
    ]
});









function mensaje(msj) {
    $('#mensajeComputed').html('<div class="alert alert-success alert-dismissible" role="alert">' +
        ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
        ' <strong>'+msj+'</strong>' +
        '</div>');
}
var tipoEntidades = new Vue({
    el:'#tipoEntidades',
    created: function () {
        this.getTipoEntidad();
    },
    data:{
        tipoEntidad: [],
        search:''
    },
    computed:{
        filteredTipoEntidad:function()
        {
            var self=this;
            return this.tipoEntidad.filter(function(tipo){
                return tipo.nombre.toLowerCase().indexOf(self.search.toLowerCase())>=0;
            });
            //return this.customers;
        }
    },
    methods:{
        getTipoEntidad: function () {
            var urlTipoEntidad = 'tipoEntidades';
            axios.get(urlTipoEntidad).then(response => {
                this.tipoEntidad = response.data.tipoEntidades;

            });
        },modalEditar: function (id,nombre) {
            $('#nombreTipoEntidadEditar').val(nombre);
            $('#idTipoEntidadEditar').val(id);
        },
        EstadoTipoEntidad: function (estado,idTipo) {
            var urlEditarTipoEntidad = 'EstadoTipoEntidad';
            axios.post(urlEditarTipoEntidad,{
                //parametros
                estado: estado,
                idTipo: idTipo
            }).then(response =>{
                tipoEntidades.getTipoEntidad();
                if (estado != 1 ){
                    mensaje('Tipo entidad deshabilitado correctamente');
                }else{
                    mensaje('Tipo entidad habilitado correctamente');
                }
            });
        }
    }
});

var crearTipoEntidad = new Vue({
    el:'#crearTipoEntidad',
    created:function () {
    } ,
    data:{
    },
    methods:{
        CrearTipoEntidad: function () {
            var urlCrearTipoEntidad = 'crearTipoEntidad';
            var nombreTipoEntidad = $('#nombreTipoEntidad').val();
            if (nombreTipoEntidad != ''){
                axios.post(urlCrearTipoEntidad,{
                    //parametros
                    nombre: nombreTipoEntidad
                }).then(response =>{
                    tipoEntidades.getTipoEntidad();
                    mensaje('Tipo entidad creado correctamente');
                    $('#myModal').modal('hide')
                    $('#nombreTipoEntidad').val('');
                });
            }else {
                $('#mensajeCreacion').html('El campo nombre no puede estar vacio!')
            }
        },

    }
});

var editarTipoEntidad = new Vue({
    el:'#editarTipoEntidad',
    created:function () {
    } ,
    data:{
    },
    methods:{
        EditarTipoEntidad: function () {
            var urlEditarTipoEntidad = 'editarTipoEntidad';
            var nombreTipoEntidadEditar = $('#nombreTipoEntidadEditar').val();
            var idTipoEntidadEditar = $('#idTipoEntidadEditar').val();
            if (nombreTipoEntidadEditar != ''){
                axios.post(urlEditarTipoEntidad,{
                    //parametros
                    nombre: nombreTipoEntidadEditar,
                    idTipo: idTipoEntidadEditar
                }).then(response =>{
                    tipoEntidades.getTipoEntidad();
                    mensaje('Tipo entidad actualizado correctamente');
                    $('#myModalEditar').modal('hide')
                });
            }else {
                $('#mensajeCreacionEditar').html('El campo nombre no puede estar vacio!')
            }
        },
    }
});