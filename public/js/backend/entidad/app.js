/**
 * Juan Carlos ALvarez Lagos
 *
**/

const router = new VueRouter({
    routes: [
        {
            path: 'entidades',component: entidades,
            path: 'tipoEntidad',component: entidades,
            path: 'crearEntidad',component: crearEntidad,
            path: 'datosEntidad',component: entidades,
        },
    ]
});

function mensaje(msj) {
    $('#mensajeComputed').html('<div class="alert alert-success alert-dismissible" role="alert">' +
        ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
        ' <strong>'+msj+'</strong>' +
        '</div>');
}

var entidades = new Vue({
    el:'#entidades',
    created:function () {
      this.getEntidades()
    } ,
    data:{
        entidades:[],
        search:'',
        datosEntidad:[]
    },

    computed:{
        filteredEntidad:function()
        {
            var self=this;
            return this.entidades.filter(function(tipo){
                return tipo.nombre.toLowerCase().indexOf(self.search.toLowerCase())>=0;
            });
            //return this.customers;
        }
    },
    methods:{
        getEntidades: function () {
            var urlEntidades = 'entidades';
            axios.get(urlEntidades).then(response => {
                this.entidades = response.data.entidades;

            });
        },
        modalEditar: function (id) {
            var urlEditar = 'datosEntidad';
            axios.post(urlEditar,{
                //parametros
                idEntidad:id
            }).then(response =>{
               this.datosEntidad = response.data.datos;
               datos = response.data.datos;
               b = response.data.tipo;
               resultado = 0;
               publica_privada =0;
                for (var i = 0 in b) {
                    resultado +='<option value="'+b[i].id_tipo_entidad+'" '+ (datos.id_tipo_entidad == b[i].id_tipo_entidad ? 'selected':'' ) +'>'+b[i].nombre+'</option>';
                   }
                   console.log(datos);
               $('#publica_privada2').html('<option value="1" '+ (datos.publica_privada == 1 ? 'selected':'')+' >Publica</option>' +
                   '<option value="2" '+ (datos.publica_privada == 2 ? 'selected':'')+'>Privada</option>');
               $('#tiposEditar').html(resultado);
               $('#idEntidadE').val(datos.id_entidad);
               $('#codigoEntidadE').val(datos.codigo);
               $('#nombreEntidadE').val(datos.nombre);
               $('#nitEntidadE').val(datos.nit);

            });
        },
        editarEntidad: function () {
            var urlEditar = 'editarEntidad';
            axios.post(urlEditar,{
                //parametros
                idEntidad: $('#idEntidadE').val(),
                codigo: $('#codigoEntidadE').val(),
                nombre: $('#nombreEntidadE').val(),
                nit: $('#nitEntidadE').val(),
                tipo: $('#tiposEditar').val(),
            }).then(response =>{
                respuesta = response.data.errors;
                if (respuesta != null && respuesta != undefined){
                    if (respuesta.codigo == null){
                        $('#codigoMsjE').html('');
                    }else {
                        $('#codigoMsjE').html(respuesta.codigo);
                    }
                    if (respuesta.nombre == null){
                        $('#nombreMsjE').html('');
                    }else {
                        $('#nombreMsjE').html(respuesta.nombre);
                    }
                    if(respuesta.nit == null){
                        $('#nitMsjE').html('');
                    }else {
                        $('#nitMsjE').html(respuesta.nit);
                    }

                }else{
                    entidades.getEntidades();
                    mensaje('Entidad Creada correctamente');
                    $('#myModalEditar').modal('hide')
                }
            });
            
        },
        EstadoEntidad: function (estado,idEntidad) {
            var urlEditar = 'estadoEntidad';
            axios.post(urlEditar,{
                //parametros
                estado: estado,
                idEntidad: idEntidad,
            }).then(response =>{
                this.getEntidades();
            });
        }
    },

});



var crearEntidad = new Vue({
   el:'#crearEntidad',
   created:function () {
       this.getTipoEntidadSelect()
   },
    data:{
      tipoEntidades:[]
    },
    methods:{
       crearEntidades:function () {
           url = 'crearEntidad';
           axios.post(url,{
               //parametros
               nombre: $('#nombreEntidad').val(),
               codigo: $('#codigoEntidad').val(),
               nit: $('#nitEntidad').val(),
               publica_privada: $('#publica_privada').val(),
               tipo_entidad: $('#tipo_entidad').val()
           }).then(response =>{
               respuesta = response.data.errors;
               if (respuesta != null && respuesta != undefined){
                   if (respuesta.codigo == null){
                       $('#codigoMsj').html('');
                   }else {
                       $('#codigoMsj').html(respuesta.codigo);
                   }
                   if (respuesta.nombre == null){
                       $('#nombreMsj').html('');
                   }else {
                       $('#nombreMsj').html(respuesta.nombre);
                   }
                   if(respuesta.nit == null){
                       $('#nitMsj').html('');
                   }else {
                       $('#nitMsj').html(respuesta.nit);
                   }

               }else{
                   entidades.getEntidades();
                   mensaje('Entidad Creada correctamente');
                   $('#myModal').modal('hide')
               }
           });
       },
        getTipoEntidadSelect:function () {
            var urlTipoEntidad = 'tipoEntidad';
            axios.get(urlTipoEntidad).then(response=>{
               this.tipoEntidades = response.data.tipoEntidades;
            });
        }
    }
});



